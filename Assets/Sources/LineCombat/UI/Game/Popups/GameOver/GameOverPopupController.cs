﻿using Tools.UI.Popup;
using UnityEngine;
using UnityEngine.UI;

namespace LineCombat.UI.Game.Popups.GameOver
{
    public sealed class GameOverPopupController:UIPopUpController
    {
        [SerializeField] private Text _text;
        private bool _isWin;

        protected override void OnStart(params object[] args)
        {
            base.OnStart(args);
            _isWin = (bool) args[0];
            _text.text = _isWin ? "You win" : "You lose";
        }
    }
}