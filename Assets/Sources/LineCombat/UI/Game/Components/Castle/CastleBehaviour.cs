﻿using System;
using LineCombat.UI.Game.Domain.Minions;
using Tools.UI;
using UnityEngine;
using UnityEngine.UI;

namespace LineCombat.UI.Game.Components.Castle
{
    sealed class CastleBehaviour : TComponentWithModel<CastleModel>, IAttackable
    {
        public event Action CastleDestroyed;

        [SerializeField] internal Slider HPBar;

        public string Id => Model.Id;

        public int HP => Model.HP;

        public void ReceiveAttack(int damage)
        {
            Model.HP -= damage;
            Model.SetChanged();
            if(Model.HP<=0)
                CastleDestroyed?.Invoke();
        }

        protected override void OnModelChanged(CastleModel model)
        {
            HPBar.value = model.HP * 1f / model.MaxHP;
        }
    }
}