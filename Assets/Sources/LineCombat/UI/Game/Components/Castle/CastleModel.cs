﻿using Tools.Core;

namespace LineCombat.UI.Game.Components.Castle
{
    internal class CastleModel:Observable
    {
        public int MaxHP { get;}

        public string Id { get; }

        public int HP { get; set; }

        public CastleModel()
        {
            MaxHP = 100;
            Id = new System.Guid().ToString();
            HP = MaxHP;
        }
    }
}