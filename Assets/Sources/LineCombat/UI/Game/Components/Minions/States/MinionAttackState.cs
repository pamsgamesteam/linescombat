﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using LineCombat.UI.Game.Domain.Minions;
using UnityEngine;
using Random = UnityEngine.Random;

namespace LineCombat.UI.Game.Components.Minions.States
{
    class MinionAttackState : MinionState
    {
        private const float kAttackDuration = 1f;
        private const int kAttackForce = 5;
        private IAttackable _taget;
        private Dictionary<string,IAttackable> _enemiesQueue = new Dictionary<string, IAttackable>();
        public override string AnimatorTriggerName
        {
            get
            {
                return "Attack";
            }
        }

        protected override void OnStart(object[] args)
        {
            base.OnStart(args);

            StartAttack((IAttackable)args[0]);
            _enemiesQueue[_taget.Id] = _taget;
            
        }

        private void StartAttack(IAttackable target)
        {
            _taget = target;

            Controller.gameObject.transform.LookAt((_taget as MonoBehaviour).transform);
            StartCoroutine(SendAttack());
        }

        protected override void OnReleaseResources()
        {
            base.OnReleaseResources();
            StopCoroutine(SendAttack());
        }

        private IEnumerator SendAttack()
        {
            yield return new WaitForSeconds(Random.Range(0f, 0.3f));

            while (true)
            {
                yield return new WaitForSeconds(kAttackDuration / 2f);

                if (Model?.HP>0&& _taget?.HP>0)
                    _taget.ReceiveAttack(kAttackForce);
                else
                    break;

                yield return new WaitForSeconds(kAttackDuration / 2f);

                if(! (_taget?.HP>0))
                    break;
            }

            if (Model?.HP > 0)
                FinishAttack();
        }

        private void FinishAttack()
        {
            if(_enemiesQueue.ContainsKey(_taget.Id))
                _enemiesQueue.Remove(_taget.Id);

            if (_enemiesQueue.Count > 0)
                StartAttack(_enemiesQueue.FirstOrDefault().Value);
            else
                StateMachine.ApplyState<MinionMoveState>();
        }

        protected override void OnEnemyEnterTrigger(Collider obj)
        {
            IAttackable target = obj.GetComponent<IAttackable>();
            _enemiesQueue[target.Id] =target;
        }

        private void OnTriggerExit(Collider obj)
        {
            IAttackable target = obj.GetComponent<IAttackable>();
            if(_enemiesQueue.ContainsKey(target.Id))
                _enemiesQueue.Remove(target.Id);
        }
    }
}
