﻿using System.Collections;
using UnityEngine;
using UnityEngine.AI;

namespace LineCombat.UI.Game.Components.Minions.States
{
    class MinionMoveState : MinionState
    {
        private static readonly Vector3 kStartPosition = new Vector3(0, -0.908f, -20f);
        private static readonly Vector3 kEnemyBasePosition = new Vector3(0, -0.908f, -0.42f);

        public override string AnimatorTriggerName
        {
            get
            {
                return "Move";
            }
        }

        protected override void OnStart(object[] args)
        {
            base.OnStart(args);
            Vector3 position = Controller.IsFirstPlayer? kEnemyBasePosition:kStartPosition;
            Agent.isStopped = false;
            Agent.SetDestination(position);
            StartCoroutine(CheckPositionReached());
        }

        protected override void OnReleaseResources()
        {
            base.OnReleaseResources();
            Agent.isStopped = true;
        }

        private IEnumerator CheckPositionReached()
        {
            yield return null;
            while (!(Agent.enabled&&Agent.remainingDistance != Mathf.Infinity && Agent.pathStatus == NavMeshPathStatus.PathComplete && Agent.remainingDistance == 0))
            {
                yield return null;
            }
           
            StateMachine.ApplyState<MinionIdleState>();
        }
    }
}
