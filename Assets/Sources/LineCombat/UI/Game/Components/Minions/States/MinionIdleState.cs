﻿namespace LineCombat.UI.Game.Components.Minions.States
{
    class MinionIdleState : MinionState
    {
        public override string AnimatorTriggerName
        {
            get
            {
                return "Idle";
            }
        }
    }
}
