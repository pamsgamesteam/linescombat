﻿using LineCombat.UI.Game.Components.Minions.Commands;
using LineCombat.UI.Game.Domain.Minions;
using UnityEngine;

namespace LineCombat.UI.Game.Components.Minions.States
{
    internal abstract class MinionState: MinionCommand
    {

        public abstract string AnimatorTriggerName { get;}

        

        protected override void OnStart(object[] args)
        {
            base.OnStart(args);

            Controller.Animator.SetTrigger(AnimatorTriggerName);

            Controller.TriggerEntered += OnEnemyEnterTrigger;
        }

        protected override void OnReleaseResources()
        {
            base.OnReleaseResources();
            Controller.TriggerEntered -= OnEnemyEnterTrigger;
        }

        protected virtual void OnEnemyEnterTrigger(Collider obj)
        {
            IAttackable attackable= obj.GetComponent<IAttackable>();
            StateMachine.ApplyState<MinionAttackState>(attackable);
        }
    }
}
