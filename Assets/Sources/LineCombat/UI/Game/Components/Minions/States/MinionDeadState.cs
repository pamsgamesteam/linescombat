﻿using System;
using System.Collections;
using UnityEngine;

namespace LineCombat.UI.Game.Components.Minions.States
{
    class MinionDeadState : MinionState
    {
        public override string AnimatorTriggerName
        {
            get
            {
                return "Die";
            }
        }

        protected override void OnStart(object[] args)
        {
            base.OnStart(args);
            GetComponent<Collider>().enabled = false;
            Agent.enabled = false;
            StartCoroutine(WaitForDie());
        }

        private IEnumerator WaitForDie()
        {
            yield return new WaitForSeconds(3f);

            Destroy(gameObject);
        }

        protected override void OnEnemyEnterTrigger(Collider obj)
        {
        }
    }
}
