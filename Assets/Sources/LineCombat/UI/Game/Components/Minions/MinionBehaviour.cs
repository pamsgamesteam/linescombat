﻿using System;
using System.Collections.Generic;
using LineCombat.UI.Game.Components.Minions.Commands;
using LineCombat.UI.Game.Components.Minions.States;
using LineCombat.UI.Game.Domain.Battleground;
using LineCombat.UI.Game.Domain.Minions;
using Tools.Core.MachineOfStates;
using Tools.UI;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;
using MinionState = LineCombat.UI.Game.Domain.Minions.MinionState;

namespace LineCombat.UI.Game.Components.Minions
{
    sealed class MinionBehaviour : TComponentWithModel<Minion>, IStateMachineContainer, IAttackable
    {

        private Dictionary<MinionState, Type> _stateToType;

        internal event Action<Collider> TriggerEntered;
        internal StateMachine StateMachine;
        [SerializeField] internal NavMeshAgent Agent;
        [SerializeField] internal Animator Animator;
        [SerializeField] internal SkinnedMeshRenderer Renderer;
        [SerializeField] internal Slider HPBar;
        public bool IsFirstPlayer;

        public MinionBehaviour()
        {
            _stateToType = new Dictionary<MinionState, Type>()
            {
                {MinionState.Idle, typeof(MinionIdleState) },
                {MinionState.Move, typeof(MinionMoveState) },
                {MinionState.Attack, typeof(MinionAttackState) },
                {MinionState.Dead, typeof(MinionDeadState) }
            };

            StateMachine = new StateMachine(this);
        }

        #region IStateMachineContainer implementation
        public GameObject GameObject => gameObject;

       
        public void Next(StateCommand previousState)
        {

        }
        #endregion

        protected override void OnModelChanged(Minion model)
        {
            Type type;
            if (_stateToType.TryGetValue(Model.State, out type))
            {
                if (StateMachine.State==null||StateMachine.State.GetType() != type)
                {
                    StateMachine.ApplyState(type);
                }
            }
        }

        public void Init(bool isFirstPlayer, MoveDirection direction)
        {
            transform.localPosition = new Vector3(0,-0.77f,0);
            IsFirstPlayer = isFirstPlayer;
            
            if (!isFirstPlayer)
            {
                if (direction == MoveDirection.LeftLine)
                    direction = MoveDirection.RightLine;
                else if (direction == MoveDirection.RightLine)
                    direction = MoveDirection.LeftLine;

                gameObject.tag = "Player2";
            }

            Agent.areaMask = (int)direction;
            StateMachine.ApplyState<MinionMoveState>();
        }

        void OnTriggerEnter(Collider other)
        {
            if (other.tag.Equals(this.gameObject.tag))
                return;

            TriggerEntered?.Invoke(other);
        }
        #region IAttackable implementation
        
        public string Id
        {
            get { return Model.Id; }
        }

        public int HP
        {
            get
            {
                return Model.HP;
            }
        }

        public void ReceiveAttack(int damage)
        {
            StateMachine.Execute<MinionReceiveDamageCommand>(damage);
        }
        #endregion
    }
}

