﻿using LineCombat.UI.Game.Components.Minions.States;

namespace LineCombat.UI.Game.Components.Minions.Commands
{
    class MinionReceiveDamageCommand:MinionCommand
    {
        protected override void OnStart(object[] args)
        {
            base.OnStart(args);

            int damage = (int)args[0];

            Model.HP -= damage;


            if(Model.HP<= 0)
            {
                StateMachine.ApplyState<MinionDeadState>();
            }

            Controller.HPBar.value = (Model.HP*1f) / Model.MaximumHp;
            FinishCommand();
        }
    }
}
