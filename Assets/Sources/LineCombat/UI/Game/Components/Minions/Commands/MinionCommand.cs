﻿using System;
using LineCombat.UI.Game.Domain.Minions;
using Tools.Core.MachineOfStates;
using UnityEngine.AI;

namespace LineCombat.UI.Game.Components.Minions.Commands
{
    internal class MinionCommand:StateCommand
    {
        private MinionBehaviour _controller;
        protected MinionBehaviour Controller
        {
            get
            {
                if (_controller)
                    return _controller;

                _controller = GetComponent<MinionBehaviour>();

                if (_controller)
                    return _controller;

                throw new Exception("Can`t find " + typeof(MinionBehaviour));
            }
        }
        protected Minion Model => Controller.Model;
        protected NavMeshAgent Agent => Controller.Agent;
        protected StateMachine StateMachine => Controller.StateMachine;
    }
}