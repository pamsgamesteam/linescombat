﻿using System;
using LineCombat.UI.Game.Domain.Battleground;
using LineCombat.UI.Game.Domain.Minions;
using Tools.UI;
using UnityEngine;
using UnityEngine.UI;

namespace LineCombat.UI.Game.Components.Minions
{
    sealed class MinionButton : TComponentWithModel<Minion>
    {
        public event Action<Minion, MoveDirection> ButtonClicked;

        [SerializeField] private Button MainButton;
        [SerializeField] private Button LeftButton;
        [SerializeField] private Button MidButton;
        [SerializeField] private Button RightButton;

        protected override void OnStart()
        {
            base.OnStart();
            MainButton.onClick.AddListener(OnMainButtonClicked);
            LeftButton.onClick.AddListener(OnLeftButtonClicked);
            MidButton.onClick.AddListener(OnMidButtonClicked);
            RightButton.onClick.AddListener(OnRightButtonClicked);

        }

        private void OnMainButtonClicked()
        {
            SetDirectionButtonActive(!LeftButton.IsActive());
        }

        private void OnLeftButtonClicked()
        {
            SetDirectionButtonActive(false);
            ButtonClicked?.Invoke(Model,MoveDirection.LeftLine);
        }

        private void OnMidButtonClicked()
        {
            SetDirectionButtonActive(false);
            ButtonClicked?.Invoke(Model, MoveDirection.MiddleLine);
        }

        private void OnRightButtonClicked()
        {
            SetDirectionButtonActive(false);
            ButtonClicked?.Invoke(Model, MoveDirection.RightLine);
        }


        protected override void OnModelChanged(Minion model)
        {
        }

        private void SetDirectionButtonActive(bool isActive)
        {
            LeftButton.gameObject.SetActive(isActive);
            MidButton.gameObject.SetActive(isActive);
            RightButton.gameObject.SetActive(isActive);
        }
    }
}
