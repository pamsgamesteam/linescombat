﻿using System;
using System.Collections.Generic;
using LineCombat.UI.Game.Components.Minions;
using LineCombat.UI.Game.Domain;
using LineCombat.UI.Game.Domain.Battleground;
using LineCombat.UI.Game.Domain.Minions;
using UnityEngine;

namespace LineCombat.UI.Game.Components
{
    internal class SpawnPoint:MonoBehaviour
    {
        private static Dictionary<Type, string> _typeToPathDictionary;

        static SpawnPoint()
        {
            _typeToPathDictionary = new Dictionary<Type, string>()
            {
                { typeof(MeleeFighterMinion),"LineCombat/UI/Game/Components/MeleeFighterMinion"}
            };
        }


        internal void Spawn(Minion minion, MoveDirection moveDirection, bool isFirstPlayer)
        {
            MinionBehaviour prefab = Resources.Load<MinionBehaviour>(_typeToPathDictionary[minion.GetType()]);

            MinionBehaviour instance = Instantiate(prefab);
            instance.transform.SetParent(transform, false);
            if(!isFirstPlayer)
            {
                instance.transform.rotation = Quaternion.Euler(new Vector3(0,180,0));
                
            }
            instance.Renderer.material.color = isFirstPlayer?Color.blue : Color.red;
            instance.Model = minion.Clone() as Minion;
            instance.Init(isFirstPlayer,moveDirection);
        }
    }
}