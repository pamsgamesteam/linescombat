﻿using LineCombat.UI.Game.Components.Castle;
using UnityEngine;

namespace LineCombat.UI.Game.Components
{
    sealed class Battleground : MonoBehaviour
    {
        [SerializeField]
        internal SpawnPoint FirstPlayerSpawnPoint;
        [SerializeField]
        internal SpawnPoint SecondPlayerSpawnPoint;
        [SerializeField]
        internal  CastleBehaviour FirstCastle;
        [SerializeField]
        internal CastleBehaviour SecondCastle;
    }
}