﻿using LineCombat.UI.Game.Domain.Battleground;
using LineCombat.UI.Game.Domain.Minions;

namespace LineCombat.UI.Game.States
{
    internal class PlayingGameState:GameState
    {
        protected override void OnStart(object[] args)
        {
            base.OnStart(args);
            Controller.FirstPlayerController.StartGame(args);
            Controller.SecondPlayerController.StartGame(args);

            Controller.FirstPlayerController.SpawnMinionRequest+= FirstPlayerControllerOnSpawnMinionRequest;
            Controller.SecondPlayerController.SpawnMinionRequest+= SecondPlayerControllerOnSpawnMinionRequest;
            Controller.View.Battleground.FirstCastle.CastleDestroyed+= OnFirstCastleDestroyed;
            Controller.View.Battleground.SecondCastle.CastleDestroyed += OnSecondCastleDestroyed;
        }

        protected override void OnReleaseResourcesWithModel()
        {
            base.OnReleaseResourcesWithModel();
            Controller.FirstPlayerController.StopGame();
            Controller.SecondPlayerController.StopGame();

            Controller.FirstPlayerController.SpawnMinionRequest -= FirstPlayerControllerOnSpawnMinionRequest;
            Controller.SecondPlayerController.SpawnMinionRequest -= SecondPlayerControllerOnSpawnMinionRequest;
            Controller.View.Battleground.FirstCastle.CastleDestroyed -= OnFirstCastleDestroyed;
            Controller.View.Battleground.SecondCastle.CastleDestroyed -= OnSecondCastleDestroyed;
        }

        private void FirstPlayerControllerOnSpawnMinionRequest(Minion minion, MoveDirection moveDirection)
        {
            View.Battleground.FirstPlayerSpawnPoint.Spawn(minion,moveDirection, true);
        }

        private void SecondPlayerControllerOnSpawnMinionRequest(Minion minion, MoveDirection moveDirection)
        {
            View.Battleground.SecondPlayerSpawnPoint.Spawn(minion, moveDirection, false);
        }

        private void OnFirstCastleDestroyed()
        {
            StateMachine.ApplyState<GameOverState>(false);
        }

        private void OnSecondCastleDestroyed()
        {
            StateMachine.ApplyState<GameOverState>(true);
        }
    }
} 