﻿using System;
using LineCombat.UI.Game.Popups.GameOver;
using Tools.UI;
using Tools.UI.Popup;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace LineCombat.UI.Game.States
{
    internal sealed class GameOverState: GameState
    {
        private bool _isFirstPlayerWin;

        protected override void OnStart(object[] args)
        {
            base.OnStart(args);
            _isFirstPlayerWin = (bool) args[0];
            (Controller.OpenPopUp<GameOverPopupController>(_isFirstPlayerWin)).Closed += OnGameOverPopupClosed;
        }

        private void OnGameOverPopupClosed(UIPopUpController popUpController)
        {
            Controller.LoadScene<GameSceneController>();
        }
    }
}