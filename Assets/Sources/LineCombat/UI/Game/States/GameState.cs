﻿using Tools.Core.MachineOfStates;

namespace LineCombat.UI.Game.States
{
    internal abstract class GameState : StateWithTypeCommand<GameSceneController>
    {
        protected GameView View => Controller.View;
        protected StateMachine StateMachine => Controller.StateMachine;
    }
}