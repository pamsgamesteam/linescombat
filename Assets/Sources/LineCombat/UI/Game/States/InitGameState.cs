﻿using System.Collections;
using LineCombat.UI.Game.Components.Castle;
using LineCombat.UI.Game.Domain.Player;
using UnityEngine;

namespace LineCombat.UI.Game.States
{
    internal sealed class InitGameState:GameState
    {
        protected override void OnStart(object[] args)
        {
            base.OnStart(args);

            CreateCastles();
            CreatePlayers(args);
            StartCoroutine(Waiter());
        }

        private IEnumerator Waiter()
        {
            yield return new WaitForSeconds(2);

            StateMachine.ApplyState<PlayingGameState>();
        }

        private void CreateCastles()
        {
            View.Battleground.FirstCastle.Model = new CastleModel();
            View.Battleground.SecondCastle.Model = new CastleModel();
        }

        private void CreatePlayers(object[] args)
        {
            Controller.FirstPlayerController = PlayerFactory.CreateRealPlayer(Controller);
            Controller.SecondPlayerController = PlayerFactory.CreateAIPlayer(Controller);
        }
    }
}