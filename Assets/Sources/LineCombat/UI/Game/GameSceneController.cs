﻿using LineCombat.UI.Game.Domain.Player;
using LineCombat.UI.Game.States;
using Tools.Core;
using Tools.UI;
using UnityEngine;

namespace LineCombat.UI.Game
{
    [UISceneName("Game")]
    sealed class GameSceneController : UISceneController
    {
        protected override IAppController AppController => LineCombat.AppController.Instance;

        [SerializeField] internal GameView View;
        [SerializeField] internal GameUIView UIView;

        internal IPlayer FirstPlayerController;
        internal IPlayer SecondPlayerController;

        private void Start()
        {
            StateMachine.ApplyState<InitGameState>();
        }

    }
}