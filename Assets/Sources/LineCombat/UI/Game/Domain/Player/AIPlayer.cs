﻿using System;
using System.Collections;
using LineCombat.UI.Game.Domain.Battleground;
using LineCombat.UI.Game.Domain.Minions;
using Tools.Utils;
using UnityEngine;
using Random = UnityEngine.Random;

namespace LineCombat.UI.Game.Domain.Player
{
    internal class AIPlayer : MonoBehaviour, IPlayer
    {
        public event Action<Minion, MoveDirection> SpawnMinionRequest;

        public void StartGame(params object[] args)
        {
            StartCoroutine(SpawnRandomMinions());
        }

        public void StopGame()
        {
            StopCoroutine(SpawnRandomMinions());
        }

        private IEnumerator SpawnRandomMinions()
        {
            while (true)
            {
                yield return new WaitForSeconds(Random.Range(0.5f, 10f));
                SpawnMinionRequest?.Invoke(new MeleeFighterMinion(), EnumUtils.RandomEnumValue<MoveDirection>());
            }
        }
    }
}