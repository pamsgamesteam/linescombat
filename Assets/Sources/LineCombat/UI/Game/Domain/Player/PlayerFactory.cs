﻿namespace LineCombat.UI.Game.Domain.Player
{
    static class PlayerFactory
    {
        public static IPlayer CreateRealPlayer(GameSceneController controller)
        {
            return controller.gameObject.AddComponent<RealPlayer>();
        }

        public static IPlayer CreateAIPlayer(GameSceneController controller)
        {
            return controller.gameObject.AddComponent<AIPlayer>();
        }
    }
}