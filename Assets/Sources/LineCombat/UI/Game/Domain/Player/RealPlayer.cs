﻿using System;
using LineCombat.UI.Game.Domain.Battleground;
using LineCombat.UI.Game.Domain.Minions;
using UnityEngine;

namespace LineCombat.UI.Game.Domain.Player
{
    internal class RealPlayer : MonoBehaviour, IPlayer
    {
        public event Action<Minion, MoveDirection> SpawnMinionRequest;

        private GameSceneController _controller;
        private GameSceneController Controller
        {
            get
            {
                if (_controller != null)
                    return _controller;

                _controller = GetComponent<GameSceneController>();
                if (_controller != null)
                    return _controller;

                _controller = FindObjectOfType<GameSceneController>();

                if (_controller != null)
                    return _controller;

                throw new NullReferenceException();
            }
        }

        public void StartGame(params object[] args)
        {
            Controller.UIView.FighterButton.ButtonClicked+= OnFighterButtonClicked;
            Controller.UIView.FighterButton.Model = new MeleeFighterMinion();
        }

        private void OnFighterButtonClicked(Minion minion, MoveDirection moveDirection)
        {
            SpawnMinionRequest?.Invoke(minion,moveDirection);
        }

        public void StopGame()
        {
            Controller.UIView.FighterButton.ButtonClicked -= OnFighterButtonClicked;
        }
    }
}