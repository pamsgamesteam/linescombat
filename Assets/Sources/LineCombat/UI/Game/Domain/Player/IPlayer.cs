﻿using System;
using LineCombat.UI.Game.Domain.Battleground;
using LineCombat.UI.Game.Domain.Minions;

namespace LineCombat.UI.Game.Domain.Player
{
    interface IPlayer
    {
        event Action<Minion, MoveDirection> SpawnMinionRequest;

        void StartGame(params object[] args);
        void StopGame();
    }
}