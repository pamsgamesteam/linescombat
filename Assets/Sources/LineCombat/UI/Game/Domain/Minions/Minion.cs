﻿using System;
using Tools.Core;

namespace LineCombat.UI.Game.Domain.Minions
{
    internal enum MinionState
    {
        Idle,
        Move,
        Attack,
        Dead
    }


    abstract class Minion:Observable, ICloneable
    {
        public MinionState State;

        public string Id { get; }
        public abstract int MaximumHp { get; }
        protected Minion()
        {
            State = MinionState.Move;
            Id = Guid.NewGuid().ToString();
            
        }

        protected int _hp { get; set; }
        

        public int HP
        {
            get
            {
                return _hp;
            }
            internal set
            {
                _hp = value;

                if (_hp < 0)
                    _hp = 0;
            }
        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}