﻿namespace LineCombat.UI.Game.Domain.Minions
{
    interface IAttackable
    {
        string Id { get; }
        int HP { get; }
        void ReceiveAttack(int kAttackForce);
    }
}
