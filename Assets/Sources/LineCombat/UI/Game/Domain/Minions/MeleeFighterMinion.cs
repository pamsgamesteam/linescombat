﻿namespace LineCombat.UI.Game.Domain.Minions
{
    sealed class MeleeFighterMinion : Minion
    {
        public override int MaximumHp { get; }

        public MeleeFighterMinion()
        {
            MaximumHp = 20;
            _hp = MaximumHp;
        }
    }
}