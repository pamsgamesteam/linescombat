﻿namespace LineCombat.UI.Game.Domain.Battleground
{
    public enum MoveDirection
    {
        LeftLine = 0x9,
        MiddleLine = 0x11,
        RightLine = 0x21
    }
}