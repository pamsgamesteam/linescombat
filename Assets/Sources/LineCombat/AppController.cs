﻿using Tools.Core;
using Tools.Utils;

namespace LineCombat
{
    public class AppController : MonoSingleton<AppController>, IAppController
    {
        public bool Inited { get; internal set; }
    }
}

